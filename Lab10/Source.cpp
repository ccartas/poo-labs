#include <iostream>
#include <string>

using namespace std;

class Device {

private:
	string deviceName;
	int bateryLevel;
	float price;

public:
	Device() {
		this->deviceName = "";
		this->bateryLevel = 0;
		this->price = 0;
	}

	Device(string deviceName, int bateryLevel, float price) {
		this->deviceName = deviceName;
		this->bateryLevel = bateryLevel;
		this->price = price;
	}

	void setBateryLevel(int value) {
		this->bateryLevel = value;
	}

	int getBateryLevel() {
		return this->bateryLevel;
	}

	void charge() {
		cout << "Device is normal charging" << endl;
		this->bateryLevel += 10;
	}

	virtual void fastCharge() {
		cout << "Device is fast charging";
		this->bateryLevel += 15;
	}
	virtual void listInstructions() = 0;
};


class SmartDevice : public Device {
private:
	int noCommands;
	string* commands;
public:
	SmartDevice() :Device() {
		this->commands = NULL;
	}

	void charge() {
		cout << "Device is fast charging wireless"<<endl;
		this->setBateryLevel(this->getBateryLevel() + 20);
	}

	void addCommand(string command) {
		string* temp = new string[this->noCommands + 1];
		for (int i = 0; i < this->noCommands; i++) {
			temp[i] = this->commands[i];
		}
		temp[this->noCommands] = command;
		this->noCommands += 1;
		if (this->commands) {
			delete[] this->commands;
		}
		this->commands = temp;
	}
	void listInstructions() {
		for (int i = 0; i < this->noCommands; i++) {
			cout << this->commands[i] << endl;
		}
	}
	void fastCharge() {
		cout << "Smart device is charging fast " << endl;
		this->setBateryLevel(this->getBateryLevel() + 25);
	}
};

class ArtificialIntelligence {
private:
	string algorithm;
public:
	ArtificialIntelligence() {
		this->algorithm = "";
	}
	virtual void applyML() = 0;
};

class HomeAssistant : public ArtificialIntelligence {
public: 
	string deviceName;
	void applyML() {
		cout << "DA";
	}
};

void main() {
	SmartDevice sd1;
	SmartDevice sd2;
	Device* devices[]{ &sd1, &sd2 };
	devices[0]->charge();
	devices[1]->charge();

	devices[0]->fastCharge();
	devices[1]->fastCharge();

	devices[1]->listInstructions();
}