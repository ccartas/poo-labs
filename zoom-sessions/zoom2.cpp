#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <iostream>
#include <string>

using namespace std;

enum class DeliveryType {
	NORMAL,
	EXPRESS,
	PRIORITAR
};


class Package {
private:
	const unsigned int uniqueId;
	float weight;
	string address;
	int noCountries;
	string* tranzit;
	DeliveryType type;
	bool isDelivered;
	static unsigned int NUMBER_OF_PACKAGES;
	static unsigned int MAX_WEIGHT;

public: 
	Package():uniqueId(NUMBER_OF_PACKAGES++) {
		this->weight = 0;
		this->address = "";
		this->noCountries = 0;
		this->tranzit = NULL;
		this->type = DeliveryType::NORMAL;
		this->isDelivered = false;
	}


	//indiferent de numarul de argumente din semnatura constructorului
	//se vor initializa toate atributele clasei
	Package(float weight, string address):uniqueId(NUMBER_OF_PACKAGES++) {
		this->weight = weight;
		this->address = address;
		this->noCountries = 0;
		this->tranzit = NULL;
		this->type = DeliveryType::NORMAL;
		this->isDelivered = false;
	}

	Package(const Package& copie) :uniqueId(NUMBER_OF_PACKAGES++) {
		this->weight = copie.weight;
		this->address = copie.address;
		this->noCountries = copie.noCountries;
		if (this->noCountries > 0) {
			this->tranzit = new string[this->noCountries];
		}
		else {
			this->tranzit = NULL;
		}

		for (int i = 0; i < this->noCountries;i++) {
			this->tranzit[i] = copie.tranzit[i];
		}

		this->type = copie.type;
		this->isDelivered = copie.isDelivered;
	}

	~Package() {
		if(this->tranzit != NULL) 
			delete[] this->tranzit;
	}

	Package& operator=(Package& copie) {
		this->weight = copie.weight;
		this->address = copie.address;
		this->noCountries = copie.noCountries;
		if (this->tranzit != NULL) {
			delete[] this->tranzit;
		}
		if (this->noCountries > 0) {
			this->tranzit = new string[this->noCountries];
		}
		else {
			this->tranzit = NULL;
		}
		for (int i = 0; i < this->noCountries;i++) {
			this->tranzit[i] = copie.tranzit[i];
		}
		this->type = copie.type;
		this->isDelivered = copie.isDelivered;
		return *this;
	}

	//PREINCREMENTARE
	Package& operator++() {
		if (this->weight + 30 <= MAX_WEIGHT) {
			this->weight += 30;
		}
		else {
			throw "Maximum weight exceded";
		}
		return *this;
	}

	//POSTINCREMENTARE
	Package operator++(int) {
		Package oldValue = *this; // apel constructor de copiere
		//this->weight += 30;
		++(*this);
		return oldValue;
	}

	//se va initializa campul isDelivered cu true doar daca pachetul a tranzitat prin
	//cel putin doua tari
	Package& operator!() {
		if (this->noCountries >= 2) {
			this->isDelivered = true;
		}
		else {
			this->isDelivered = false;
		}
		/*if (this->isDelivered == false) {
			this->isDelivered = true;
		}
		else {
			this->isDelivered = false;
		}*/
		//this->isDelivered = !this->isDelivered;
		return *this;
	}

	// { China, SUA, Bg, Romania}
	Package& operator+=(string country) {
		//aloc memorie pentru un nou vector de dimensiune curenta + 1
		string* temp = new string[this->noCountries + 1];
		//copiez elementele existente in vectorul tranzit in vectorul nou(temp)
		for (int i = 0; i < this->noCountries; i++) {
			temp[i] = this->tranzit[i];
		}
		//adaug pe ultima pozitie din noul vector tara pasata ca si parametru
		temp[this->noCountries] = country;
		this->noCountries += 1;
		if (this->tranzit != NULL) {
			delete[] this->tranzit;
		}
		this->tranzit = temp;
		// SAU (NU RECOMAND - ESTE REDUNDANT)
		/*
		this->tranzit = new string[this->noCountries];
		for(int i = 0; i<this->noCountries; i++) {
			this->tranzit[i] = temp[i];
		}
		*/
		return *this;
	}

	//operatorul index
	string& operator[](int index) {
		if (index >= 0 && index < this->noCountries) {
			return this->tranzit[index];
		}
		else {
			throw "Index out of bounds.";
		}
	}

	//cast implicit
	operator int() const {
		return this->noCountries;
	}

	//cast explicit
	explicit operator int() {
		return this->address.length();
	}

	friend ostream& operator<<(ostream& out, const DeliveryType& type);
	friend ostream& operator<<(ostream& out, const Package& package);
	friend istream& operator>>(istream& in, Package& package);
};

unsigned int Package::NUMBER_OF_PACKAGES = 0;
unsigned int Package::MAX_WEIGHT = 500;

ostream& operator<<(ostream& out, const DeliveryType& type) {
	/*switch (type) {
	case DeliveryType::NORMAL:
		out << "NORMAL";
		break;
	case DeliveryType::EXPRESS:
		out << "EXPRESS";
		break;
	case DeliveryType::PRIORITAR:
		out << "PRIORITAR";
		break;
	default:
		out << "NORMAL";
	}*/
	if (type == DeliveryType::NORMAL) {
		out << "NORMAL";
	}
	else if (type == DeliveryType::EXPRESS) {
		out << "EXPRESS";
	}
	else if (type == DeliveryType::PRIORITAR) {
		out << "PRIORITAR";
	}
	else {
		out << "NORMAL";
	}

	return out;
}

ostream& operator<<(ostream& out, const Package& package) {
	out << endl << "Weight: " << package.weight;
	out << endl << "Address: " << package.address;
	out << endl << "No Countries: "<< package.noCountries;
	for (int i = 0; i < package.noCountries;i++) {
		out << endl << package.tranzit[i];
	}
	out << endl << "Delivery Type: "<< package.type;
	out << endl << "Is Delivered: "<<package.isDelivered;
	return out;
}

istream& operator>>(istream& in, Package& package) {
	cout << "Weight: ";
	in >> package.weight;
	cout << "Address: ";
	in >> package.address;
	cout << "No Countries: ";
	in >> package.noCountries;
	if (package.tranzit != NULL) {
		delete[] package.tranzit;
	}
	if (package.noCountries > 0) {
		package.tranzit = new string[package.noCountries];
	}
	else {
		package.tranzit = NULL;
	}
	for (int i = 0; i < package.noCountries; i++) {
		in >> package.tranzit[i];
	}
	//initializarea valorii package.type putea fi facuta prin supraincarcarea operatorului
	//>> pentru enumul DeliveryType (vezi <<)
	int type = 0;
	in >> type;
	if (type == 0) {
		package.type = DeliveryType::NORMAL;
	}
	else if (type == 1) {
		package.type = DeliveryType::EXPRESS;
	}
	else if (type == 2) {
		package.type = DeliveryType::PRIORITAR;
	}
	else {
		package.type = DeliveryType::NORMAL;
	}
	//pana aici
	in >> package.isDelivered;
	return in;
}


void main() {
	Package p1;
	Package p2 = p1;
	Package p3;
	cin >> p3;
	//cout << p1 << p2 << p3;

	cout << endl << "Testare Operator = " << endl;
	p1 = p3;
	cout << p1;
	
	int i = 0;

	cout << endl << "Testare preincrementare ++ " << endl;
	cout << ++p1;
	
	cout << endl << "Testare postincrementare ++" << endl;
	cout << p1++;

	cout << endl << "Testare operator += " << endl;

	p1 += "China";
	p1 += "SUA";

	cout << p1;

	cout << endl << p1[0];
	p1[0] = "Japonia";
	cout << endl << p1[0];
	
	cout << endl << "Testare operator cast implicit " << endl;
	int x = p1;
	cout << endl << x;

	cout << endl << "Testare operator cast explicit " << endl;
	int y = (int) p1;
	cout << endl << y;
}