#include <iostream>
#include <string>

using namespace std;

class BankAccount {
private:
	const int ID;
	string owner;
	int noTransactions;
	float* transactions;
public:
	static int numberOfAccounts;
	BankAccount();
	BankAccount(const char* owner, int noTransactions, float* transactions);
	BankAccount(const BankAccount& copy);
	~BankAccount();

	
	

	BankAccount& operator=(const BankAccount& acc);
	/*
	* Suprascrieti operatorul + pentru efectuarea unei noi tranzactii pentru un cont bancar.
	* b1 + 5000 ++ [] cast implicit si explicit !
	*/
	BankAccount operator+(float amount);
	//b1 += 3000
	BankAccount& operator+=(float amount);

	//Operatorul ! va fi folosit in felul urmator: !b1 si va verifica daca vectorul de tranzactii este initializat
	bool operator!();


	//apel: b1++;
	BankAccount operator++(int);
	BankAccount& operator++();

	//operatorul va calcula totalul tranzactiilor
	//pentru a face diferenta dintre cele doua se va adauga la semnatura operatorului implicit keyword-ul const
	//cast implicit
	operator float() const {
		float total = 0;
		for (int i = 0; i < this->noTransactions; i++) {
			total += this->transactions[i];
		}
		return total;
	}
	//cast explicit
	explicit operator float() {
		float total = 0;
		for (int i = 0; i < this->noTransactions; i++) {
			total += this->transactions[i];
		}
		return total;
	}

	float operator[](int index) {
		if (index >= 0 && index < this->noTransactions) {
			return this->transactions[index];
		}
		else {
			throw "Invalid index";
		}
	}

	friend ostream& operator<<(ostream& out,const BankAccount& acc);
	void setNoTransactions(int noTransactions) {
		if (noTransactions > 0) {
			this->noTransactions = noTransactions;
		}
		else {
			throw "Eroare la validare";
		}
	}
};



BankAccount::BankAccount() :ID(numberOfAccounts) {
	this->owner = "John Doe";
	this->noTransactions = 0;
	this->transactions = NULL;
	numberOfAccounts++;
}

BankAccount::BankAccount(const char* owner, int noTransactions, float* transactions) :ID(numberOfAccounts) {
	this->owner = owner;
	this->noTransactions = noTransactions;
	this->transactions = new float[this->noTransactions];
	for (int i = 0; i < this->noTransactions; i++) {
		this->transactions[i] = transactions[i];
	}
	numberOfAccounts++;
}

BankAccount::BankAccount(const BankAccount& copy):ID(numberOfAccounts) {
	this->owner = copy.owner;
	this->noTransactions = copy.noTransactions;
	this->transactions = new float[this->noTransactions];
	for (int i = 0; i < this->noTransactions; i++) {
		this->transactions[i] = copy.transactions[i];
	}
	numberOfAccounts++;
}

BankAccount::~BankAccount() {
	if (this->transactions != NULL) {
		delete[] this->transactions;
	}
}

ostream& operator<<(ostream& out, const BankAccount& acc) {
	out << acc.owner << " cu ID-ul: " << acc.ID << " a efectuat " << acc.noTransactions << " tranzactii:"<<endl;
	for (int i = 0; i < acc.noTransactions; i++) {
		out << acc.transactions[i] << endl;
	}
	return out;
}

BankAccount& BankAccount::operator=(const BankAccount& acc) {
	if (this->transactions != NULL) {
		delete[] this->transactions;
	}
	this->owner = acc.owner;
	this->noTransactions = acc.noTransactions;
	this->transactions = new float[this->noTransactions];
	for (int i = 0; i < this->noTransactions; i++) {
		this->transactions[i] = acc.transactions[i];
	}
	
	return *this;
}
/*
* Operatorul + nu ar trebui sa intoarca referinta pentru ca el va returna rezultatul operatiei de adunare.
* Cum rezultatul este un nou obiect creat cu ajutorul constructorului de copiere, acesta trebuie sa ocupe o zona noua de memorie.
*/
BankAccount BankAccount::operator+(float amount) {
	BankAccount resultAcc = *this;
	if (resultAcc.transactions != NULL) {
		float* tempTransactions = new float[resultAcc.noTransactions + 1];
		for (int i = 0; i < resultAcc.noTransactions; i++) {
			tempTransactions[i] = resultAcc.transactions[i];
		}
		tempTransactions[resultAcc.noTransactions] = amount;
		if(resultAcc.transactions != NULL)
			delete[] resultAcc.transactions;
		resultAcc.transactions = tempTransactions;
		resultAcc.noTransactions++;
	}
	else {
		resultAcc.noTransactions = 1;
		resultAcc.transactions = new float[resultAcc.noTransactions];
		resultAcc.transactions[0] = amount;
	}
	return resultAcc;
}

/*
* Operatorul += poate fi implementat foarte simplu daca am implementat ulterior operatorul +
* Aveti comentat si varianta in care nu a fost implementat anterior +-ul
*/
BankAccount& BankAccount::operator+=(float amount) {
	/*
	float* tempTransactions = new float[this->noTransactions + 1];
	for (int i = 0; i < this->noTransactions; i++) {
		tempTransactions[i] = this->transactions[i];
	}
	tempTransactions[this->noTransactions] = amount;
	if (this->transactions != NULL)
		delete[] this->transactions;
	this->transactions = tempTransactions;
	this->noTransactions++;
	*/
	*this = *this + amount;
	return *this;
}

//La fel ca la operatorul +, post incrementarea va crea o noua zona de memorie i++ este echivalent cu i = i+1;
//Pentru a face diferenta intre cele doua metode de incrementare, metoda de postincrementare are adaugat un argument artifiial de tip in
BankAccount BankAccount::operator++(int) {
	BankAccount copy = *this + 0.0f;
	return copy;
}

BankAccount& BankAccount::operator++() {
	float* tempTransactions = new float[this->noTransactions + 1];
	for (int i = 0; i < this->noTransactions; i++) {
		tempTransactions[i] = this->transactions[i];
	}
	tempTransactions[this->noTransactions] = 0;
	if (this->transactions != NULL)
		delete[] this->transactions;
	this->transactions = tempTransactions;
	this->noTransactions++;
	return *this;
}

bool BankAccount::operator!() {
	if (this->transactions == NULL) {
		return false;
	}
	else {
		return true;
	}
}

//operatorul cast implicit va calcula totalul tranzactiilor

int BankAccount::numberOfAccounts = 0;

void main() {
	BankAccount b1;
	BankAccount b2("Gigel Ionel", 2, new float[2]{ 3000, 5000 });
	BankAccount b3 = b2; //apel constructor copiere
	cout << b1;
	cout << b2;
	cout << b3;
	b1 = b3; // apel operator =
	cout << b3;
	b2 += 2000;
	cout << b2;
	BankAccount b5 = b1 + 2000.0f;
	cout << b5;
	try {
		b2.setNoTransactions(-1);
	}
	catch (const char* exception) {
		cout << exception;
	}
	cout << endl << !b1;
	cout << b2++;

	float total = b2; // cast implicit
	float total_explicit = (float) b2; //cast explicit
	cout << endl << total;
	cout << endl << total_explicit;

	try {
		cout << endl<< b2[2];
	}
	catch (const char* ex) {
		cout << ex;
	}
}