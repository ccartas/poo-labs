#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif


#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class Order {
private:
	const int orderId;
	int noItems;
	float* prices;
	string address;
	static int NUMBER_OF_ORDERS;
public:
	Order():orderId(NUMBER_OF_ORDERS++) {
		this->noItems = 0;
		this->prices = NULL;
		this->address = "";
	}

	Order(string address):orderId(NUMBER_OF_ORDERS++) {
		this->address = address;
		this->prices = NULL;
		this->noItems = 0;
	}


	explicit operator float() {
		float MAX = 0;
		for (int i = 0; i < this->noItems;i++) {
			if (MAX < this->prices[i]) {
				MAX = this->prices[i];
			}
		}
		return MAX;
	}

	float& operator[](int index) {
		if (index >= 0 && index < this->noItems) {
			return this->prices[index];
		}
		else {
			throw "Index out of bounds";
		}
	}

	
	friend ostream& operator<<(ostream& out, const Order& o);
	friend istream& operator>>(istream& in, Order& o);

	friend ofstream& operator<<(ofstream& out, const Order& o);
	friend ifstream& operator>>(ifstream& in, Order& o);
};
int Order::NUMBER_OF_ORDERS = 0;


ostream& operator<<(ostream& out, const Order& o) {
	out << o.address << endl;
	out << o.noItems << endl;
	for (int i = 0;i < o.noItems;i++) {
		out << o.prices[i] << endl;
	}

	return out;
}

istream& operator>>(istream& in, Order& o) {
	in >> o.address;
	in >> o.noItems;
	if (o.prices != NULL) {
		delete[] o.prices;
	}
	o.prices = new float[o.noItems];
	for (int i = 0;i < o.noItems;i++) {
		in >> o.prices[i];
	}
	return in;
}


ofstream& operator<<(ofstream& out, const Order& o) {
	int noCharacters = o.address.length();
	out.write((char*)&noCharacters, sizeof(int));
	out.write(&o.address[0], noCharacters);
	out.write((char*)&o.noItems, sizeof(int));
	for (int i = 0; i < o.noItems; i++) {
		out.write((char*)&o.prices[i], sizeof(float));
	}
	return out;
}

ifstream& operator>>(ifstream& in, Order& o) {
	int noCharacters = 0;

	in.read((char*)&noCharacters, sizeof(int));
	o.address.resize(noCharacters);
	in.read(&o.address[0], noCharacters);
	in.read((char*)&o.noItems, sizeof(int));
	if (o.prices != NULL) {
		delete[] o.prices;
	}
	o.prices = new float[o.noItems];
	for (int i = 0; i < o.noItems; i++) {
		in.read((char*)&o.prices[i], sizeof(float));
	}
	return in;
}

void main() {
	Order o1;
	Order o2;
	cin >> o2;
	ofstream file;
	file.open("orders.txt", ofstream::out | ofstream::trunc | ofstream::binary );
	file << o2;
	file.close(); 

	ifstream file1;
	file1.open("orders.txt", ifstream::in | ifstream::binary);
	file1 >> o1;
	file1.close();
	cout << o1;
}