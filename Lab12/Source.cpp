#include <iostream>

using namespace std;


void printNumber(int number) {
	cout << number * 10<<endl;
}

template <class TYPE>
void printNumberTemplate(TYPE number) {
	cout << number * 10 << endl;
}

template <class TYPE, int SIZE>
class Vector {
private:
	TYPE* array;
	int size;
	static int numberOfElements;
public: 

	Vector() {
		this->size = SIZE;
		this->array = new TYPE[this->size];
	}
	
	void add(const TYPE& item) {
		if (numberOfElements < this->size) {
			numberOfElements++;
			this->array[numberOfElements - 1] = item;
		}
		else {
			TYPE* temp = new TYPE[this->size + 1];
			for (int i = 0; i < this->size; i++) {
				temp[i] = this->array[i];
			}
			numberOfElements++;
			temp[this->size] = item;
			this->size += 1;
			this->array = temp;
		}
	}

	void display() {
		for (int i = 0; i < numberOfElements; i++) {
			cout << this->array[i] << endl;
		}
	}

	TYPE& operator[](int index) {
		if (index >= 0 && index < this->size) {
			return this->array[index];
		}
		else {
			throw "Index out of bounds.";
		}
	}

	template<class T, int SIZE>
	friend ostream& operator<<(ostream& out, const Vector<T, SIZE>& vector);
};


template<class TYPE, int SIZE>
ostream& operator<<(ostream& out, const Vector<TYPE, SIZE>& vector) {
	for (int i = 0; i < vector.numberOfElements; i++) {
		out << vector.array[i] << endl;
	}
	return out;
}

template<class TYPE, int SIZE>
int Vector<TYPE, SIZE>::numberOfElements = 0;

void main() {
	/* printNumber(20);
	printNumberTemplate(10.5);
	printNumberTemplate(20); */

	Vector<int, 2> array;
	array.add(5);
	array.display();
	array.add(7);
	array.display();
	array.add(7);
	array.display();

	cout << array;
}