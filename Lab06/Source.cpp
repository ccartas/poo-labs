#include <iostream>
#include <string>

using namespace std;

class Person {
private:
	const string CNP;
	string name;
	string surname;
	int months;
	float* costs;
	static int noPersons;

public:
	Person(const char* CNP, const char* name, const char* surname, int months, float* costs):CNP(CNP) {
		this->name = name;
		this->surname = surname;
		this->months = months;
		this->costs = new float[this->months];
		for (int i = 0; i < this->months; i++) {
			this->costs[i] = costs[i];
		}
	}
	void setName(const char* name) {
		if (strlen(name) < 3) {
			throw exception("eroare");
		}
		else {
			this->name = name;
		}
		
	}
	~Person() {
		if (this->costs != NULL) {
			delete[] this->costs;
		}
	}

	Person& operator=(Person& copy) {
		this->name = copy.name;
		this->surname = copy.surname;
		this->months = copy.months;
		this->costs = new float[this->months];
		for (int i = 0; i < this->months; i++) {
			this->costs[i] = copy.costs[i];
		}
		return *this;
	}

	friend ostream& operator<<(ostream& out, Person& p);
	friend void operator>>(istream& in, Person& p);

	bool operator>(Person& p) {
		float sumP1 = 0;
		float sumP2 = 0;
		for (int i = 0; i < this->months; i++) {
			sumP1 += this->costs[i];
		}
		for (int i = 0; i < p.months; i++) {
			sumP2 += this->costs[i];
		}
		return sumP1 > sumP2;
	}


	float operator[](int index) {
		return this->costs[index];
	}

	int operator()() {
		return this->months;
	}

	explicit operator int() {
		return this->months;
	}
};

int Person::noPersons = 0;

ostream& operator<<(ostream& out, Person& p) {
	out << p.name << " " << p.surname;
	return out;
}

void operator>>(istream& in, Person& p) {
	cout << "Introduceti numele persoanei: ";
	in >> p.name;
	cout << endl << "Introduceti prenumele";
	in >> p.surname;
	cout << endl << "Numarul de luni: ";
	in >> p.months;
	p.costs = new float[p.months];
	for (int i = 0; i < p.months; i++) {
		cout << endl << "Costul " << i + 1;
		in >> p.costs[i];
	}
}

void main() {
	Person p1("2938213327642384", "Gigi", "Popescu", 3, new float[3]{ 2000, 5000, 7000 });
	Person p2("2938213327642384", "Gigel", "Ionel", 3, new float[3]{ 2000, 5000, 8000 });
	//cin >> p1;
	cout << p1;
	p2 = p1;
	try {
		p1.setName("da");
	}
	catch(exception& e) {
		cout << e.what();
	}
}