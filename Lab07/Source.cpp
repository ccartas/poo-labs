#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;
class InstagramAccount {
private:
	string username;
	int noPosts;
	int* likes;

public:
	InstagramAccount() {
		this->username = "";
		this->noPosts = 0;
		this->likes = NULL;
	}

	InstagramAccount(string username, int noPosts, int* likes) {
		this->username = username;
		this->noPosts = noPosts;
		this->likes = new int[this->noPosts];
		for (int i = 0; i < this->noPosts; i++) {
			this->likes[i] = likes[i];
		}
	}
	int getTotalLikes() {
		int total = 0;
		for (int i = 0; i < this->noPosts; i++) {
			total += this->likes[i];
		}
		return total;
	}

	string getUserName() {
		return this->username;
	}

	int getNoPosts() {
		return this->noPosts;
	}

	int* getLikes() {
		return this->likes;
	}
};
void createInstagramReport(string fileName, InstagramAccount* accounts, int noAccounts) {
	ofstream file;
	file.open(fileName, ofstream::out | ofstream::trunc);
	if (file.is_open()) {
		file << noAccounts << endl;
		for (int i = 0; i < noAccounts; i++) {
			file << accounts[i].getUserName() << endl;
			file << accounts[i].getNoPosts() << endl;
			for (int j = 0; j < accounts[i].getNoPosts(); j++) {
				file << accounts[i].getLikes()[j] << endl;
			}
		}
		cout << "Report generated successfully" << endl;
		file.close();
	}
	else {
		throw "Report could not be generated";
	}
}

InstagramAccount* readReport(string fileName) {
	ifstream file;
	InstagramAccount* accounts;
	file.open(fileName, ifstream::in);
	if (file.is_open()) {
		int noAccounts;
		file >> noAccounts;
		accounts = new InstagramAccount[noAccounts];
		int accNumber = 0;
		while (!file.eof() && accNumber < noAccounts) {
			string accountName;
			int noPosts;
			int* likes;
			file >> accountName;
			cout << accountName << endl;
			file >> noPosts;
			likes = new int[noPosts];
			for (int i = 0; i < noPosts; i++) {
				file >> likes[i];
				cout << likes[i] << endl;
			}
			accNumber++;
		}
		file.close();
	}
	else {

	}
	return accounts;
}


void main() {
	InstagramAccount acc1("foodie.lover", 3, new int[3]{ 25,50,1000 });
	InstagramAccount acc2("tech.savy", 2, new int[2]{ 500, 100 });
	createInstagramReport("report.txt", new InstagramAccount[2]{ acc1, acc2 }, 2);
	readReport("report.txt");

	InstagramAccount* accs = new InstagramAccount[2]{ acc1, acc2 };
	InstagramAccount* restored = NULL;
	int noAccs = 0;
	
	for (int i = 0; i < noAccs; i++) {
		cout << restored[i].getUserName() << endl;
	}
}