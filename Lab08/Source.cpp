#include <iostream>
#include <string>
#include <fstream>

using namespace std;

enum class HTTPMethod {
	GET,
	POST,
	PUT,
	DELETE
};


string convertEnumToString(HTTPMethod& method) {
	string result;
	switch (method) {
	case HTTPMethod::GET:
		result = "GET";
		return result;
	case HTTPMethod::POST:
		result = "POST";
		return result;
	case HTTPMethod::PUT:
		result = "PUT";
		return result;
	case HTTPMethod::DELETE:
		result = "DELETE";
		return result;
	default:
		result = "";
		return result;
	}
}

class HTTPRequest {
private:
	const char* SESSION_ID;
	string URL;
	string httpMethod;
	char* requestBody;
public:
	HTTPRequest();
	HTTPRequest(const char* SESSION_ID, string URL, HTTPMethod method, const char* body);
};

HTTPRequest::HTTPRequest() :SESSION_ID("DEFAULT") {
	this->URL = "";
	this->httpMethod = "";
	this->requestBody = NULL;
}

HTTPRequest::HTTPRequest(const char* SESSION_ID, string URL, HTTPMethod method, const char* body) :SESSION_ID(SESSION_ID) {
	this->URL = URL;
	this->httpMethod = convertEnumToString(method);
	this->requestBody = new char[strlen(body) + 1];
	strcpy(this->requestBody, body);
}

void writeBinary(string fileName, HTTPRequest* requests, int noRequests) {
	ofstream file;
	file.open(fileName, ofstream::out | ofstream::trunc | ofstream::binary);
	if (file.is_open()) {
		for (int i = 0; i < noRequests; i++) {
			file.write((char*)&requests[i], sizeof(HTTPRequest));
		}
		file.close();
		cout << "Data was serialized" << endl;
	}
}

void readBinary(string fileName, HTTPRequest* &requests, int& noRequests) {
	ifstream file;
	file.open(fileName, ifstream::in | ifstream::binary);
	if (file.is_open()) {
		file.seekg(0, fstream::end);
		int fileSize = file.tellg();
		noRequests = fileSize / sizeof(HTTPRequest);
		requests = new HTTPRequest[noRequests];
		file.seekg(0, fstream::beg);
		for (int i = 0; i < noRequests; i++) {
			file.read((char*)&requests[i], sizeof(HTTPRequest));
		}
		file.close();
		cout << "Deserialization success" << endl;
	}
}

void main() {
	HTTPRequest req1("WQERES12131212", "http://localhost:8080/register", HTTPMethod::POST, "username+password");
	HTTPRequest req2("WQERES12874322", "http://localhost:8080/login", HTTPMethod::POST, "user+pass");
	HTTPRequest* requests = new HTTPRequest[2]{ req1, req2 };
	int noRequests = 0;
	HTTPRequest* restored = NULL;
	writeBinary("data.dat", requests, 2);
	readBinary("data.dat", restored, noRequests);
	cout << "Process complete" << endl;
}