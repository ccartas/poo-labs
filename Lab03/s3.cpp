// Lab03.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <iostream>

using namespace std;

class ElectricVehicle {
private:
	char* make;
	char* model;
	
	double price;
public:
	static int numberOfCars;
	const int year;
	ElectricVehicle();
	ElectricVehicle(const char* make, const char* model,int year, double price);
};

int ElectricVehicle::numberOfCars = 0;

ElectricVehicle::ElectricVehicle():year(2000){
	
}

ElectricVehicle::ElectricVehicle(const char*  make, const char* model, int year, double price):year(year){
	this->make = new char[strlen(make) + 1];
	strcpy(this->make, make);
	this->model = new char[strlen(model) + 1];
	strcpy(this->model, model);
	this->price = price;
}


int main()
{
	ElectricVehicle ev1 = ElectricVehicle("Tesla", "Model 3", 2019, 60000);
	cout << ev1.year;
	ev1.year = 2020;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
