#include <iostream>;
#include <string>;

using namespace std;

class Student {
private:
	string nume; //char*
	string prenume; //char* 
	int nrNote;
	int* note;

public:
	static int nrStudenti;

	Student() {
		this->nume = "";
		this->prenume = "";
		this->nrNote = 1;
		this->note = new int[this->nrNote];
		for (int i = 0; i < this->nrNote; i++) {
			this->note[i] = 10;
		}
		nrStudenti++;
	}

	Student(const char* nume, const char* prenume, int nrNote, int* note) {
		this->nume = nume;
		this->prenume = prenume;
		this->nrNote = nrNote;
		this->note = new int[this->nrNote];
		for (int i = 0; i < this->nrNote; i++) {
			this->note[i] = note[i];
		}
		nrStudenti++;
	}

	Student(const Student& copy) {
		this->nume = copy.nume;
		this->prenume = copy.prenume;
		this->nrNote = copy.nrNote;
		this->note = new int[this->nrNote];
		for (int i = 0; i < this->nrNote; i++) {
			this->note[i] = copy.note[i];
		}
		nrStudenti++;
	}

	Student& operator=(Student& copy) {
		this->nume = copy.nume;
		this->prenume = copy.prenume;
		this->nrNote = copy.nrNote;
		if (this->note != NULL) {
			delete[] this->note;
		}
		this->note = new int[this->nrNote];
		for (int i = 0; i < this->nrNote; i++) {
			this->note[i] = copy.note[i];
		}
		return *this;
	}
	~Student() {
		if (this->note != NULL) {
			delete[] this->note;
		}
		nrStudenti--;
	}
	void afisareStudent() {
		cout << this->nume << " " << this->prenume << " are " << this->nrNote << " note:" << endl;
		for (int i = 0; i < this->nrNote; i++) {
			cout << this->note[i]<<endl;
		}
		cout << "numarul de studenti " << nrStudenti << endl;
	}
};
int Student::nrStudenti = 0;
void main() {
	Student s1;
	s1.afisareStudent();
	Student s2("Gigel", "Ionel", 3, new int[3]{ 10, 9, 10 });
	s2.afisareStudent();
	Student s3 = s2; //apel constructor de copiere
	s1 = s2; // shallow copy
	s1 = s2 = s3; //s1 = (s2 = s3)
	s1.afisareStudent();
}