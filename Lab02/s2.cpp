// Lab02.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <iostream>

using namespace std;

typedef struct student {
	char* nume_student;
	int numar_materii;
	int* note;
} student;


student initialize_student() {
	char buffer[100];
	int numar_note;
	student s;
	cout << "Introduceti numele studentului: ";
	cin >> buffer;
	s.nume_student = new char[strlen(buffer) + 1];
	strcpy(s.nume_student, buffer);
	cout << "Introduceti numarul de materii: ";
	cin >> numar_note;
	s.numar_materii = numar_note;
	s.note = new int[s.numar_materii];
	for (int i = 0; i < s.numar_materii; i++) {
		cout << "Introduceti nota pentru disciplina " << i + 1 << ": ";
		cin >> s.note[i];
	}
	return s;
}

void afisare_structura(student &s) {
	cout << sizeof(s);
	cout << " Numele studentului este " << s.nume_student;
	cout << " Notele studentului sunt: ";
	for (int i = 0; i < s.numar_materii; i++) {
		cout << " " << s.note[i];
	}
}

void afisare_structura_ptr(student *s) {
	cout << sizeof(s);
	cout << " Numele studentului este " << s->nume_student;
	cout << " Notele studentului sunt: ";
	for (int i = 0; i < s->numar_materii; i++) {
		cout << " " << s->note[i];
	}
}

int main()
{
	student s1 = initialize_student();
	afisare_structura(s1);
	afisare_structura_ptr(&s1);
}
