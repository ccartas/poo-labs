// Lab04.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

class SmartDevice {
private: 
	const char* deviceId;
	char* deviceName;
	int bateryCapacity;
	double price;
public:
	SmartDevice();
	SmartDevice(const char* deviceId, const char* deviceName, int batery, double price);
	SmartDevice(const SmartDevice& copy);
	SmartDevice(const SmartDevice& copy, const char* deviceId);
	
	void setDeviceName(const char* deviceName);
	void setBateryCapacity(int bateryCapacity);
	void setPrice(double price);

	const char* getDeviceId();
	char* getDeviceName();
	int getBateryCapacity();
	double getPrice();
};

SmartDevice::SmartDevice() {
	this->deviceName = new char[strlen("Alexa") + 1];
	strcpy(this->deviceName, "Alexa");
	this->bateryCapacity = 4000;
	this->price = 1000;
}

SmartDevice::SmartDevice(const char* deviceId, const char* deviceName, int capacity, double price):deviceId(deviceId){
	this->deviceName = new char[strlen(deviceName) + 1];
	strcpy(this->deviceName, deviceName);
	this->bateryCapacity = capacity;
	this->price = price;
}

SmartDevice::SmartDevice(const SmartDevice& copy, const char* deviceId):deviceId(deviceId){
	if (copy.deviceName != NULL) {
		this->deviceName = new char[strlen(copy.deviceName) + 1];
		strcpy(this->deviceName, copy.deviceName);
	}
	this->bateryCapacity = copy.bateryCapacity;
	this->price = copy.price;
}

SmartDevice::SmartDevice(const SmartDevice& copy) {
	cout << "Apel constructor copiere";
	if (copy.deviceName != NULL) {
		this->deviceName = new char[strlen(copy.deviceName) + 1];
		strcpy(this->deviceName, copy.deviceName);
	}
	this->bateryCapacity = copy.bateryCapacity;
	this->price = copy.price;
}

void SmartDevice::setDeviceName(const char* deviceName) {
	if (deviceName != NULL) {
		if (strlen(deviceName) >= 4) {
			this->deviceName = new char[strlen(deviceName) + 1];
			strcpy(this->deviceName, deviceName);
		}
	}
	else {
		throw 500;
	}
}

void SmartDevice::setBateryCapacity(int bateryCapacity) {
	if (bateryCapacity > 0) {
		this->bateryCapacity = bateryCapacity;
	}
	else {
		throw 500;
	}
}

int SmartDevice::getBateryCapacity() {
	return this->bateryCapacity;
}

int main() {
	SmartDevice device = SmartDevice("Device1", "Google Home", 4000, 5000);
	SmartDevice device1 = device; //apel constructor de copiere;
	SmartDevice device2(device1, "Some device id"); //apel explicit constructor de copiere
	device1.setDeviceName("Apple Home Pad");
	cout << device1.getBateryCapacity();
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
