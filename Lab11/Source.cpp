#include <iostream>
#include <vector>
#include <list>
#include <string>

using namespace std;

class PiesaAuto {
public:
	string denumire;
	const int ID_PIESA;
	static int NUMAR_PIESE_CREATE;


	PiesaAuto(): ID_PIESA(NUMAR_PIESE_CREATE++){
		this->denumire = "";
	}

	PiesaAuto(string denumire) :ID_PIESA(NUMAR_PIESE_CREATE++) {
		this->denumire = denumire;
	}

	PiesaAuto& operator=(PiesaAuto& copy) {
		this->denumire = copy.denumire;
		cout << "Apel operator =" << endl;
		return *this;
	}

	//ostream& operator<<()
};

int PiesaAuto::NUMAR_PIESE_CREATE = 0;

class Masina {
private:
	PiesaAuto* piese;
	int nrPiese;
public: 
	Masina() {
		this->nrPiese = 0;
		this->piese = NULL;
	}

	Masina(PiesaAuto* piese, int nrPiese) {
		this->nrPiese = nrPiese;
		this->piese = new PiesaAuto[this->nrPiese];
		for (int i = 0; i < this->nrPiese; i++) {
			this->piese[i] = piese[i];
		}
	}
	
	Masina& operator+=(PiesaAuto& pa) {
		PiesaAuto* temp = new PiesaAuto[this->nrPiese+1];
		for (int i = 0; i < this->nrPiese; i++) {
			temp[i] = this->piese[i];
		}
		temp[this->nrPiese] = pa;
		this->nrPiese += 1;
		if (this->piese != NULL) {
			delete[] this->piese;
		}

		this->piese = new PiesaAuto[this->nrPiese];
		this->piese = temp;
		return *this;
	}

	friend ostream& operator<<(ostream& out, Masina& m);
};

ostream& operator<<(ostream& out, Masina& m) {
	for (int i = 0; i < m.nrPiese; i++) {
		out << m.piese[i].denumire << endl;
	}
	return out;
}

class MasinaElectrica {
private:
	list <PiesaAuto> piese;
public:
	MasinaElectrica() {
		this->piese = {};
	}

	MasinaElectrica(list<PiesaAuto> piese) {
		this->piese = piese;
	}

	MasinaElectrica& operator+=(PiesaAuto& pa) {
		this->piese.push_back(pa);
		return *this;
	}

	friend ostream& operator<<(ostream& out, MasinaElectrica& me);

};

ostream& operator<<(ostream& out, MasinaElectrica& m) {
	list<PiesaAuto>::iterator iterator;
	for (iterator = m.piese.begin(); iterator != m.piese.end(); iterator++) {
		out << (*iterator).denumire;
	}
	return out;
}

void main() {
	Masina m1(new PiesaAuto[2]{ PiesaAuto("Motor"), PiesaAuto("Sasiu") }, 2);
	PiesaAuto p1("Curea Distributie");
	m1 += p1;
	cout << m1;

	MasinaElectrica me1({ PiesaAuto("Cutie Viteza"), PiesaAuto("Turbina") });
	me1 += p1;
	cout << me1;
}