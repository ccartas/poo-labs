#include <iostream>
#include <string>

using namespace std;

class Vehicle {
protected:
	string make;
	string model;
	const int year;
public:
	Vehicle():year(2000) {
		this->make = "";
		this->model = "";
	}
	Vehicle(const int year, string make, string model):year(year) {
		this->make = make;
		this->model = model;
	}
};

class NormalVehicle : Vehicle {
private:
	string fuel;
public:
	NormalVehicle() :Vehicle() {
		this->fuel = "GAS";
	}
	NormalVehicle(string type):Vehicle() {

	}
};

class ElectricVehicle : Vehicle {
private:
	const int BATERY_CAPACITY;
	int range;
public: 
	ElectricVehicle():BATERY_CAPACITY(100), Vehicle() {
		this->range = 0;
	}
	ElectricVehicle(const int capacity, const int year,
		string make,
		string model,
		int range) :BATERY_CAPACITY(capacity), Vehicle(year, make, model) {
		this->range = range;
	}

	friend ostream& operator <<(ostream& out, ElectricVehicle& vehicle);
};

ostream& operator<<(ostream& out, ElectricVehicle& vehicle) {
	out << vehicle.make << " " << vehicle.model << " " << vehicle.BATERY_CAPACITY << " " << vehicle.range;
	return out;
}

void main() {
	ElectricVehicle ev1(100000, 2000, "Tesla", "Model 3", 400);
	cout << ev1;
}