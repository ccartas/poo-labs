// Lab01.cpp : This file contains the 'main' function. Program execution begins and ends there.

#include <iostream>

using namespace std;

void increment(double a) {
	cout << "Adresa lui a din functia increment " << &a << endl;
	a++;
}

void increment_adresa(double &a) {
	cout << "Adresa lui a din functia increment_adresa " << &a << endl;
	a++;
}

void increment_pointer(double *a) {
	cout << "Adresa lui a din functia increment_pointer " << &a << endl;
	(*a)++;
}

int main()
{
	double a = 0;
	cout << "Adresa la care a fost creat a:" << &a <<endl;
	cout << "Valoarea lui a inainte de incrementare este: " << a << endl;
	increment(a);
	cout << "Valoarea lui a dupa incrementarea 1 este: "<< a << endl;
	increment_adresa(a);
	cout << "Valoarea lui a dupa incrementarea 2 este: " << a << endl;
	increment_pointer(&a);
	cout << "Valoarea lui a dupa incrementarea 3 este: " << a << endl;
}
